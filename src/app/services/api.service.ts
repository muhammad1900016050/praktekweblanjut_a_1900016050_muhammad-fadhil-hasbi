import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ApiService {
  serverUrl: any = 'https://api.sunhouse.co.id/bookstore/index.php/';
  constructor(public http: HttpClient) {}

  //Get Service
  httpOptions: any;
  getToken() {
    var tokenKey = localStorage.getItem('appToken');
    if (tokenKey != null) {
      var tkn = JSON.parse(tokenKey);
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + tkn.token,
        }),
      };
    }
  }

  //Post Service
  httpOptions2: any;
  postToken() {
    var tokenKey = localStorage.getItem('appToken');
    if (tokenKey != null) {
      var tkn = JSON.parse(tokenKey);
      this.httpOptions2 = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + tkn.token,
        }),
      };
    }
  }

  //Put Service
  httpOptions3: any;
  putToken() {
    var tokenKey = localStorage.getItem('appToken');
    if (tokenKey != null) {
      var tkn = JSON.parse(tokenKey);
      this.httpOptions3 = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + tkn.token,
        }),
      };
    }
  }

  //Delete Service
  httpOptions4: any;
  deleteToken() {
    var tokenKey = localStorage.getItem('appToken');
    if (tokenKey != null) {
      var tkn = JSON.parse(tokenKey);
      this.httpOptions4 = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + tkn.token,
        }),
      };
    }
  }

  get(url: any) {
    this.getToken();
    return this.http.get(this.serverUrl + url, this.httpOptions);
  }
  post(url: any, data: any) {
    this.postToken();
    return this.http.post(this.serverUrl + url, data, this.httpOptions2);
  }
  put(url: any, data: any) {
    this.putToken();
    return this.http.put(this.serverUrl + url, data, this.httpOptions3);
  }
  delete(url: any) {
    this.deleteToken();
    return this.http.delete(this.serverUrl + url, this.httpOptions4);
  }

  //Login
  login(email: any, password: any) {
    return this.http.post(this.serverUrl + 'auth/login', {
      email: email,
      password: password,
    });
  }
  //Register
  register(email: any, password: any) {
    return this.http.post(this.serverUrl + 'auth/register', {
      email: email,
      password: password,
    });
  }

  // Upload File
  upload(file: any) {
    return this.http.post(this.serverUrl + 'upload/book', file);
  }
}
